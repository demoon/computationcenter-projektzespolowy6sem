package computationCenter;

public final class Task {
    private transient int startTime;
    private final transient int length;

    public Task(final int length) {
        if (length < 0) {
            throw new IllegalArgumentException();
        }

        this.length = length;
        this.startTime = -1;
    }

    public void allocateTo(final int aStart) {
        if (aStart < 0) {
            throw new IllegalArgumentException();
        }
        this.startTime = aStart;
    }

    public void moveForwardInTime() {
        this.startTime++;
    }

    public int getLength() {
        return this.length;
    }

    public boolean isAllocated() {
        if (this.startTime != -1) {
            return true;
        }
        return false;
    }

    public int getEndTime() {
        if (!isAllocated()) {
            throw new IllegalStateException();
        }
        return this.startTime + this.length;
    }

    public int getStartTime() {
        if (!isAllocated()) {
            throw new IllegalStateException();
        }
        return this.startTime;
    }

    @Override
    public String toString() {
        return String.valueOf(this.length);
    }

}
