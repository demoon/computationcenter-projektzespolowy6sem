package computationCenter;

public final class AllJobsBuilder {
    private transient int minimalProcessorNumber = 1;
    private transient int maximalProcessorNumber;
    private transient int minimalLength = 1;
    private transient int maximalLength;
    private transient int numberOfJobs;

    public AllJobsBuilder() {
    }

    public AllJobs buildAllJobs() {
        return new AllJobs(this.minimalProcessorNumber, this.maximalProcessorNumber, this.minimalLength,
                this.maximalLength, this.numberOfJobs);
    }

    public AllJobsBuilder minimalProcessorNumber(final int minimalProcessorNumber) {
        this.minimalProcessorNumber = minimalProcessorNumber;
        return this;
    }

    public AllJobsBuilder maximalProcessorNumber(final int maximalProcessorNumber) {
        this.maximalProcessorNumber = maximalProcessorNumber;
        return this;
    }

    public AllJobsBuilder minimalLength(final int minimalLength) {
        this.minimalLength = minimalLength;
        return this;
    }

    public AllJobsBuilder maximalLength(final int maximalLength) {
        this.maximalLength = maximalLength;
        return this;
    }

    public AllJobsBuilder numberOfJobs(final int numberOfJobs) {
        this.numberOfJobs = numberOfJobs;
        return this;
    }

}
