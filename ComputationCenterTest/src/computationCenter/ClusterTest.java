package computationCenter;

import junit.framework.Assert;

import org.junit.Test;

public class ClusterTest {

    @Test
    public void testCanYouAdd_0() {
        final Cluster c = new Cluster(7);
        final JobPackage jp = new JobPackage(3, 3);
        jp.allocateTo(2, 2);
        c.add(jp);
        Assert.assertTrue(c.canYouAdd(0, 1, 2, 4));
    }

    @Test
    public void testCanYouAdd_1() {
        final Cluster c = new Cluster(7);
        final JobPackage jp = new JobPackage(3, 3);
        jp.allocateTo(2, 2);
        c.add(jp);
        Assert.assertTrue(c.canYouAdd(1, 6, 0, 2));
    }

    @Test
    public void testCanYouAdd_2() {
        final Cluster c = new Cluster(7);
        final JobPackage jp = new JobPackage(3, 3);
        jp.allocateTo(2, 2);
        c.add(jp);
        Assert.assertTrue(c.canYouAdd(5, 5, 2, 4));
    }

    @Test
    public void testCanYouAdd_3() {
        final Cluster c = new Cluster(7);
        final JobPackage jp = new JobPackage(3, 3);
        jp.allocateTo(2, 2);
        c.add(jp);
        Assert.assertTrue(c.canYouAdd(4, 6, 5, 8));
    }

    @Test
    public void testCanYouAdd_4() {
        final Cluster c = new Cluster(7);
        final JobPackage jp = new JobPackage(3, 3);
        jp.allocateTo(2, 2);
        c.add(jp);
        Assert.assertFalse(c.canYouAdd(0, 3, 2, 4));
    }

    @Test
    public void testCanYouAdd_5() {
        final Cluster c = new Cluster(7);
        final JobPackage jp = new JobPackage(3, 3);
        jp.allocateTo(2, 2);
        c.add(jp);
        Assert.assertFalse(c.canYouAdd(1, 6, 0, 3));
    }

    @Test
    public void testCanYouAdd_6() {
        final Cluster c = new Cluster(7);
        final JobPackage jp = new JobPackage(3, 3);
        jp.allocateTo(2, 2);
        c.add(jp);
        Assert.assertFalse(c.canYouAdd(4, 5, 2, 4));
    }

    @Test
    public void testCanYouAdd_7() {
        final Cluster c = new Cluster(7);
        final JobPackage jp = new JobPackage(3, 3);
        jp.allocateTo(2, 2);
        c.add(jp);
        Assert.assertFalse(c.canYouAdd(4, 6, 4, 8));
    }

    @Test
    public void testCanYouAdd_8() {
        final Cluster c = new Cluster(7);
        final JobPackage jp = new JobPackage(3, 3);
        jp.allocateTo(2, 2);
        c.add(jp);
        Assert.assertFalse(c.canYouAdd(3, 3, 3, 4));
    }

    @Test
    public void testCanYouAdd_9() {
        final Cluster c = new Cluster(7);
        final JobPackage jp = new JobPackage(3, 3);
        jp.allocateTo(2, 2);
        c.add(jp);
        Assert.assertFalse(c.canYouAdd(0, 6, 0, 6));
    }

    @Test(expected = IllegalStateException.class)
    public void testAdd_0() {
        final Cluster c = new Cluster(7);
        final JobPackage jp = new JobPackage(-2, 2);
        c.add(jp);
    }

    @Test(expected = IllegalStateException.class)
    public void testAdd_1() {
        final Cluster c = new Cluster(7);
        final JobPackage jp = new JobPackage(332, 4);
        c.add(jp);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAdd_2() {
        final Cluster c = new Cluster(7);
        final JobPackage jp = new JobPackage(2, -4);
        c.add(jp);
    }

}
