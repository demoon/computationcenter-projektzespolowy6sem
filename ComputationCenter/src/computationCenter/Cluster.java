package computationCenter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public final class Cluster implements Iterable<Processor>, Iterator<Processor> {
    private int count = 0;
    private final List<Processor> cluster = new ArrayList<Processor>();

    public Cluster(final int numberOfProcessors) {
        if (numberOfProcessors < 0) {
            throw new IllegalArgumentException();
        }

        for (int i = 0; i < numberOfProcessors; i++) {
            this.cluster.add(new Processor());
        }
    }

    public void add(final JobPackage jp) {
        if (!canYouAdd(jp)) {
            throw new IllegalStateException();
        }
        final Task task = jp.getTask();
        for (int i = jp.getStartProcessor(); i <= jp.getLastProcessor(); i++) {
            this.cluster.get(i).add(jp.getTaskStart(), task);
        }
    }

    public boolean canYouAdd(final int firstProcessor, final int lastProcessor, final int processingStart,
            final int processingEnd) {
        if ((firstProcessor < 0) || (firstProcessor > (this.cluster.size() - 1))) {
            throw new IllegalArgumentException();
        }

        if ((lastProcessor < 0) || (lastProcessor > (this.cluster.size() - 1))) {
            throw new IllegalArgumentException();
        }

        if ((processingStart < 0) || (processingEnd < 0)) {
            throw new IllegalArgumentException();
        }

        for (int i = firstProcessor; i <= lastProcessor; i++) {
            if (this.cluster.get(i).isBusyBetween(processingStart, processingEnd)) {
                return false;
            }
        }
        return true;
    }

    public boolean canYouAdd(final JobPackage jp) {
        return canYouAdd(jp.getStartProcessor(), jp.getLastProcessor(), jp.getTaskStart(), jp.getTaskEnd());
    }

    public int getNumberOfProcessors() {
        return this.cluster.size();
    }

    public int finishTimeOfCluster() {
        int finishTimeOfCluster = 0;
        for (final Processor processor : this.cluster) {
            final int finishTimeOfCurrentProcessor = processor.finishTimeOfLastTask();

            if (finishTimeOfCurrentProcessor > finishTimeOfCluster) {
                finishTimeOfCluster = finishTimeOfCurrentProcessor;
            }

        }
        return finishTimeOfCluster;
    }

    public int getClusterArea() {
        return finishTimeOfCluster() * getNumberOfProcessors();
    }

    @Override
    public String toString() {
        String s = "";
        for (int i = 0; i < this.cluster.size(); i++) {
            s += "Processor " + i + " " + this.cluster.get(i);
        }
        return s;
    }

    @Override
    public Iterator<Processor> iterator() {
        return this;
    }

    @Override
    public boolean hasNext() {
        if (this.count < this.cluster.size()) {
            return true;
        }
        this.count = 0;
        return false;
    }

    @Override
    public Processor next() {
        if (this.count >= this.cluster.size()) {
            throw new NoSuchElementException();
        }
        this.count++;
        return this.cluster.get(this.count - 1);

    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

}
