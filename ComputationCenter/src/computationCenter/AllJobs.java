package computationCenter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public final class AllJobs implements Iterable<JobPackage>, Iterator<JobPackage> {
    private final transient List<JobPackage> list = new ArrayList<JobPackage>();
    private int count = 0;

    public AllJobs(final int minimalProcessorNumber, final int maximalProcessorNumber, final int minimalLength,
            final int maximalLength, final int numberOfJobs) {

        for (int i = 0; i < numberOfJobs; i++) {
            this.list.add(JobPackage.createRandomPackage(minimalProcessorNumber, maximalProcessorNumber, minimalLength,
                    maximalLength));
        }

        Collections.sort(this.list);
        Collections.reverse(this.list);

    }

    public int getNumberOfJobPackages() {
        return this.list.size();
    }

    @Override
    public Iterator<JobPackage> iterator() {
        return this;
    }

    @Override
    public boolean hasNext() {
        if (this.count < this.list.size()) {
            return true;
        }
        this.count = 0;
        return false;

    }

    @Override
    public JobPackage next() {
        if (this.count == this.list.size()) {
            throw new NoSuchElementException();
        }

        this.count++;
        return this.list.get(this.count - 1);
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String toString() {
        String s = "";
        for (final JobPackage jp : this.list) {
            s += jp;
        }
        return s;
    }

}
