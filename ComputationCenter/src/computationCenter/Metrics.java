package computationCenter;

public interface Metrics {
    public int compute(Cluster cluster);
}
