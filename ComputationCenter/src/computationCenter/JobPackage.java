package computationCenter;

import java.util.Random;

public final class JobPackage implements Comparable<JobPackage> {
    private final transient Task task;
    private transient int startProcessor;
    private final transient int requiredProcessors;
    private static final int NOT_ALLOCATED = -1;

    public JobPackage(final int requiredProcessors, final int taskLength) {
        this.task = new Task(taskLength);
        this.requiredProcessors = requiredProcessors;
        this.startProcessor = JobPackage.NOT_ALLOCATED;
    }

    private void checkAllocation() {
        if ((this.startProcessor == JobPackage.NOT_ALLOCATED) || !this.task.isAllocated()) {
            throw new IllegalStateException();
        }
    }

    public void allocateTo(final int startProcessor, final int startTime) {
        if ((startProcessor < 0) || (startTime < 0)) {
            throw new IllegalArgumentException();
        }

        this.startProcessor = startProcessor;
        this.task.allocateTo(startTime);
    }

    public void moveToNextProcessor() {
        this.startProcessor++;
    }

    public void moveForwardInTime() {
        this.task.moveForwardInTime();
    }

    public void resetProcessor() {
        this.startProcessor = 0;
    }

    public boolean isUsedOnProcessor(final int processorNumber) {
        checkAllocation();

        int lastProcessor;
        if (this.startProcessor != 0) {
            lastProcessor = (this.requiredProcessors + this.startProcessor) - 1;
        }
        else {
            lastProcessor = this.requiredProcessors + this.startProcessor;
        }
        final boolean isLaterThanStart = this.startProcessor <= processorNumber;
        final boolean isEarlierThanEnd = processorNumber <= lastProcessor;
        return isLaterThanStart && isEarlierThanEnd;
    }

    public Task getTask() {
        return this.task;
    }

    public int getTaskStart() {
        checkAllocation();
        return this.task.getStartTime();
    }

    public int getTaskEnd() {
        checkAllocation();
        return this.task.getEndTime();
    }

    public int getStartProcessor() {
        checkAllocation();

        return this.startProcessor;
    }

    public int getLastProcessor() {
        checkAllocation();
        return (this.startProcessor + this.requiredProcessors) - 1;
    }

    public int getArea() {
        return this.requiredProcessors * this.task.getLength();
    }

    public int getRequredProcessors() {
        return this.requiredProcessors;
    }

    public int getTaskLength() {
        return this.task.getLength();
    }

    @Override
    public int compareTo(final JobPackage anotherJP) {
        final int currentArea = this.requiredProcessors * this.task.getLength();
        final int otherArea = anotherJP.requiredProcessors * anotherJP.task.getLength();
        if (currentArea > otherArea) {
            return 1;
        }
        else if (currentArea == otherArea) {
            return 0;
        }
        else {
            return -1;
        }
    }

    public static JobPackage createRandomPackage(final int minProcessor, final int maxProcessor, final int minLength,
            final int maxLength) {
        final Random rand = new Random();
        final int processors = rand.nextInt((maxProcessor - minProcessor) + 1) + minProcessor;
        final int length = rand.nextInt((maxLength - minLength) + 1) + minLength;
        return new JobPackage(processors, length);
    }

    @Override
    public String toString() {
        return "JobPackage [time = " + getTaskLength() + " processors = " + getRequredProcessors() + " ]\n";
    }
}
