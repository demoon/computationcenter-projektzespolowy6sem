package computationCenter;

public class ProcessorsWithIdleTime implements Metrics {

    @Override
    public int compute(final Cluster cluster) {
        int processorsWithIdleTime = 0;
        final int finishTimeOfCluster = cluster.finishTimeOfCluster();

        for (final Processor processor : cluster) {
            if (processor.getLongestIdleTimeUntil(finishTimeOfCluster) != 0) {
                processorsWithIdleTime++;
            }
        }
        return processorsWithIdleTime;
    }
}
