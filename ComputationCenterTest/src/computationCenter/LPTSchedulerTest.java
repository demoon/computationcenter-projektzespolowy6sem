package computationCenter;

import org.junit.Test;

public class LPTSchedulerTest {

    @Test
    public void test() {
        final LPTSchedulerBuilder schedulerBuilder = new LPTSchedulerBuilder();
        final LPTScheduler scheduler = schedulerBuilder.maximalLength(100).maximalProcessorNumber(10).numberOfJobs(3)
                .numberOfProcessors(11).build();
        scheduler.schedule();
    }

}
