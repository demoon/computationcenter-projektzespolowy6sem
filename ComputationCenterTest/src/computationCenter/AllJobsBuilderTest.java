package computationCenter;

import junit.framework.Assert;

import org.junit.Test;

public class AllJobsBuilderTest {

    @Test
    public void test() {
        final AllJobsBuilder ajb = new AllJobsBuilder();
        final AllJobs aj = ajb.maximalLength(10).minimalLength(1).minimalProcessorNumber(1).maximalProcessorNumber(10)
                .numberOfJobs(10).buildAllJobs();
        Assert.assertTrue(aj.getNumberOfJobPackages() == 10);

    }

}
