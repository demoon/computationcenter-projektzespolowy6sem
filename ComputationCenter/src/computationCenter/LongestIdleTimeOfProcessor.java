package computationCenter;

public class LongestIdleTimeOfProcessor implements Metrics {

    @Override
    public int compute(final Cluster cluster) {
        final int finishTimeOfCluster = cluster.finishTimeOfCluster();

        int maxIdleTime = 0;
        for (final Processor processor : cluster) {
            final int currentIdleTime = processor.getLongestIdleTimeUntil(finishTimeOfCluster);
            if (currentIdleTime > maxIdleTime) {
                maxIdleTime = currentIdleTime;
            }
        }

        return maxIdleTime;
    }

}
