package computationCenter;

import java.util.LinkedList;
import java.util.List;

public final class Processor {
    private final transient List<Task> tasks;

    public Processor() {
        this.tasks = new LinkedList<Task>();
    }

    public void add(final int startTime, final Task singleTask) {
        singleTask.allocateTo(startTime);
        int placeForNewTask = 0;
        final int newTaskEnd = startTime + singleTask.getLength();

        if (this.tasks.isEmpty()) {
            this.tasks.add(singleTask);
            return;
        }

        if (this.tasks.get(this.tasks.size() - 1).getEndTime() <= singleTask.getStartTime()) {
            this.tasks.add(singleTask);
            return;
        }
        for (int i = this.tasks.size() - 1; i >= 0; i--) {
            if (newTaskEnd < this.tasks.get(i).getStartTime()) {
                placeForNewTask = i;
                break;
            }
        }
        this.tasks.add(placeForNewTask, singleTask);

    }

    public int finishTimeOf(final int taskNumber) {
        return this.tasks.get(taskNumber).getEndTime();
    }

    public int finishTimeOfLastTask() {
        if (this.tasks.isEmpty()) {
            return 0;
        }
        return this.tasks.get(this.tasks.size() - 1).getEndTime();
    }

    public int startTimeOf(final int taskNumber) {
        return this.tasks.get(taskNumber).getStartTime();
    }

    public boolean isBusyBetween(final int start, final int end) {

        // no tasks on list
        if (this.tasks.isEmpty()) {
            return false;
        }

        // requested region is located at the end or beginning of the list
        if ((this.tasks.get(this.tasks.size() - 1).getEndTime() <= start) || (end <= this.tasks.get(0).getStartTime())) {
            return false;
        }

        // requested region is inside a list
        for (int i = this.tasks.size() - 1; i >= 0; i--) {
            if (end <= this.tasks.get(i).getStartTime()) {
                if (this.tasks.get(i - 1).getEndTime() <= start) {
                    return false;
                }
                return true;
            }
        }
        return true;
    }

    public Task getTask(final int taskNumber) {
        return this.tasks.get(taskNumber);
    }

    public int getLongestIdleTimeUntil(final int endTime) {
        if (this.tasks.isEmpty()) {
            return endTime;
        }

        int longestIdleTime = 0;
        int initialTime = 0;
        int currentEmptySpace = 0;
        for (final Task task : this.tasks) {
            currentEmptySpace = task.getStartTime() - initialTime;
            if (currentEmptySpace > longestIdleTime) {
                longestIdleTime = currentEmptySpace;
            }
            initialTime = task.getEndTime();
        }

        if ((endTime - finishTimeOfLastTask()) > longestIdleTime) {
            longestIdleTime = endTime - finishTimeOfLastTask();
        }
        return longestIdleTime;
    }

    @Override
    public String toString() {
        return this.tasks + "\n";
    }

}
