package computationCenter;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Algorithm {
    private static final int MAXIMAL_TASK_LENGTH = 50;
    private static final int MAXIMAL_PROCESSORS_USED_BY_TASK = 50;
    private static final int NUMBER_OF_JOBS = 100;
    private static final int PROCESSORS = 51;
    private static final int MAX_ITERATIONS = 10000;
    private static final int MIN_RANDOM_PROCESSOR_INCREASE = 1;
    private static final int MAX_RANDOM_PROCESSOR_INCREASE = 20;
    private static int bestArea = Integer.MAX_VALUE;

    public static void main(final String[] args) {
        final LPTSchedulerBuilder schedulerBuilder = new LPTSchedulerBuilder();
        final LPTScheduler scheduler = schedulerBuilder.maximalLength(Algorithm.MAXIMAL_TASK_LENGTH)
                .maximalProcessorNumber(Algorithm.MAXIMAL_PROCESSORS_USED_BY_TASK)
                .numberOfJobs(Algorithm.NUMBER_OF_JOBS).numberOfProcessors(Algorithm.PROCESSORS).build();
        for (int iteration = 0; iteration < Algorithm.MAX_ITERATIONS; iteration++) {
            scheduler.schedule();

            final int currentArea = scheduler.getArea();
            if (currentArea < Algorithm.bestArea) {
                Algorithm.bestArea = currentArea;
                final Logger logger = LoggerFactory.getLogger(Algorithm.class.getSimpleName());
                logger.info("Iteration: {} Current area is equal to {}", iteration, Algorithm.bestArea);
            }

            final Random rand = new Random();
            final int processorsToChange = rand
                    .nextInt((Algorithm.MAX_RANDOM_PROCESSOR_INCREASE - Algorithm.MIN_RANDOM_PROCESSOR_INCREASE) + 1)
                    + Algorithm.MIN_RANDOM_PROCESSOR_INCREASE;
            if (scheduler.getLongestIdleTimeOfProcessor() < scheduler.getProcessorsWithIdleTime()) {
                scheduler.decreaseProcessorNumber(processorsToChange);
            }
            else {
                scheduler.increaseProcessorNumber(processorsToChange);
            }
        }
    }
}
