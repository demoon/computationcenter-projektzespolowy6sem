package computationCenter;

import org.junit.Assert;
import org.junit.Test;

/**
 * The class <code>ProcessorTest</code> contains tests for the class
 * <code>{@link Processor}</code>.
 * 
 * @generatedBy CodePro at 15/04/12 00:10
 * @author d
 * @version $Revision: 1.0 $
 */
public class ProcessorTest {
    @Test
    public void testAdd() {
        final Processor p = new Processor();
        final Task t1 = new Task(3);
        final Task t2 = new Task(3);

        p.add(0, t1);
        p.add(4, t2);

        Assert.assertTrue(p.finishTimeOf(0) == 3);
    }

    @Test
    public void testAdd_1() {
        final Processor p = new Processor();
        final Task t1 = new Task(3);
        final Task t2 = new Task(3);

        p.add(1, t1);
        p.add(4, t2);

        Assert.assertTrue(p.startTimeOf(1) == 4);
    }

    @Test
    public void testAdd_2() {
        final Processor p = new Processor();
        final Task t1 = new Task(3);
        final Task t2 = new Task(3);

        p.add(0, t1);
        p.add(4, t2);

        Assert.assertTrue(p.getTask(1) == t2);
    }

    @Test
    public void testIsBusyBetween() {
        final Processor p = new Processor();
        final Task t1 = new Task(3);
        final Task t2 = new Task(3);

        p.add(1, t1);
        p.add(5, t2);

        Assert.assertFalse(p.isBusyBetween(0, 1));
    }

    @Test
    public void testIsBusyBetween_1() {
        final Processor p = new Processor();
        final Task t1 = new Task(3);
        final Task t2 = new Task(3);

        p.add(1, t1);
        p.add(5, t2);

        Assert.assertTrue(p.isBusyBetween(1, 2));
    }

    @Test
    public void testIsBusyBetween_2() {
        final Processor p = new Processor();
        final Task t1 = new Task(3);
        final Task t2 = new Task(3);

        p.add(1, t1);
        p.add(5, t2);

        Assert.assertTrue(p.isBusyBetween(3, 5));
    }

    @Test
    public void testIsBusyBetween_3() {
        final Processor p = new Processor();
        final Task t1 = new Task(3);
        final Task t2 = new Task(3);

        p.add(1, t1);
        p.add(5, t2);

        Assert.assertFalse(p.isBusyBetween(4, 5));
    }

    @Test
    public void testIsBusyBetween_4() {
        final Processor p = new Processor();
        final Task t1 = new Task(3);
        final Task t2 = new Task(3);

        p.add(1, t1);
        p.add(5, t2);

        Assert.assertTrue(p.isBusyBetween(4, 6));
    }

    @Test
    public void testIsBusyBetween_5() {
        final Processor p = new Processor();
        final Task t1 = new Task(3);
        final Task t2 = new Task(3);

        p.add(1, t1);
        p.add(5, t2);

        Assert.assertTrue(p.isBusyBetween(5, 8));
    }

    @Test
    public void testIsBusyBetween_6() {
        final Processor p = new Processor();
        final Task t1 = new Task(3);
        final Task t2 = new Task(3);

        p.add(1, t1);
        p.add(5, t2);

        Assert.assertTrue(p.isBusyBetween(7, 8));
    }

    @Test
    public void testIsBusyBetween_7() {
        final Processor p = new Processor();
        final Task t1 = new Task(3);
        final Task t2 = new Task(3);

        p.add(1, t1);
        p.add(5, t2);

        Assert.assertFalse(p.isBusyBetween(8, 9));
    }

    @Test
    public void testIsBusyBetween_8() {
        final Processor p = new Processor();
        final Task t1 = new Task(3);
        final Task t2 = new Task(3);

        p.add(1, t1);
        p.add(5, t2);

        Assert.assertTrue(p.isBusyBetween(0, 9));
    }

}
