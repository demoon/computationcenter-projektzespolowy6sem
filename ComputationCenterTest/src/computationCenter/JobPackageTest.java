package computationCenter;

import junit.framework.Assert;

import org.junit.Test;

public class JobPackageTest {

    @Test
    public void testIsUsedOnProcessor() {
        final JobPackage jp = new JobPackage(3, 2);
        jp.allocateTo(1, 2);
        Assert.assertTrue(jp.isUsedOnProcessor(2));
    }

    @Test
    public void testIsUsedOnProcessor_1() {
        final JobPackage jp = new JobPackage(3, 2);
        jp.allocateTo(1, 2);
        Assert.assertFalse(jp.isUsedOnProcessor(0));
    }

    @Test
    public void testIsUsedOnProcessor_2() {
        final JobPackage jp = new JobPackage(3, 2);
        jp.allocateTo(1, 2);
        Assert.assertFalse(jp.isUsedOnProcessor(4));
    }

    @Test
    public void testCompareTo() {
        final JobPackage jp = new JobPackage(3, 3);
        final JobPackage jp2 = new JobPackage(3, 3);
        Assert.assertTrue(jp.compareTo(jp2) == 0);
    }

    @Test
    public void testCompareTo_1() {
        final JobPackage jp = new JobPackage(3, 3);
        final JobPackage jp2 = new JobPackage(3, 4);
        Assert.assertTrue(jp.compareTo(jp2) == -1);
    }

    @Test
    public void testCompareTo_2() {
        final JobPackage jp = new JobPackage(5, 3);
        final JobPackage jp2 = new JobPackage(3, 4);
        Assert.assertTrue(jp.compareTo(jp2) == 1);
    }

    @Test
    public void testCreateRandomPackage() {

        final JobPackage jp = JobPackage.createRandomPackage(1, 5, 2, 6);
        jp.allocateTo(1, 2);

        Assert.assertTrue(jp.getStartProcessor() >= 1);
        Assert.assertTrue(jp.getLastProcessor() <= 6);
        Assert.assertTrue(jp.getTaskStart() >= 2);
        Assert.assertTrue(jp.getTaskEnd() <= 8);
    }

}
