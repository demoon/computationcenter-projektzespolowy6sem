package computationCenter;

import junit.framework.Assert;

import org.junit.Test;

public class AllJobsTest {

    @Test
    public void testNumberOfJobPackages() {
        final AllJobs aj = new AllJobsBuilder().minimalProcessorNumber(1).maximalProcessorNumber(4).minimalLength(1)
                .maximalLength(10).buildAllJobs();

        // final AllJobs aj = new AllJobs(1, 4, 1, 10, 3);
        Assert.assertTrue(aj.getNumberOfJobPackages() == 3);
    }

    @Test
    public void testSorting() {
        final AllJobs aj = new AllJobs(1, 4, 1, 10, 30);

        int previousArea = Integer.MAX_VALUE;
        for (final JobPackage jp : aj) {
            Assert.assertTrue(previousArea >= jp.getArea());
            previousArea = jp.getArea();
        }
    }

    @Test
    public void testIteratorAccess() {
        final AllJobs aj = new AllJobs(1, 4, 1, 10, 3);

        for (final JobPackage current : aj) {
            current.allocateTo(1, 1);

            Assert.assertTrue(current.getStartProcessor() >= 1);
            Assert.assertTrue(current.getLastProcessor() <= 5);
            Assert.assertTrue(current.getTaskStart() >= 1);
            Assert.assertTrue(current.getTaskEnd() <= 11);
        }
    }

}
