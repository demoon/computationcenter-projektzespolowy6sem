package computationCenter;

import org.junit.Assert;
import org.junit.Test;

public class TaskTest {

    @Test
    public void testIsAllocated() {
        final Task t = new Task(3);

        Assert.assertFalse(t.isAllocated());
    }

    @Test
    public void testIsAllocated_1() {
        final Task t = new Task(3);
        t.allocateTo(5);

        Assert.assertTrue(t.isAllocated());
    }
}
