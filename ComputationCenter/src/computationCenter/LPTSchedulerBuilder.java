package computationCenter;

public final class LPTSchedulerBuilder {
    private transient int minimalProcessorNumber = 1;
    private transient int maximalProcessorNumber = 1;
    private transient int minimalLength = 1;
    private transient int maximalLength = 1;
    private transient int numberOfJobs = 1;
    private transient int numberOfProcessors = 1;

    public LPTSchedulerBuilder() {

    }

    public LPTScheduler build() {
        return new LPTScheduler(this.minimalProcessorNumber, this.maximalProcessorNumber, this.minimalLength,
                this.maximalLength, this.numberOfJobs, this.numberOfProcessors);
    }

    public LPTSchedulerBuilder minimalProcessorNumber(final int minimalProcessorNumber) {
        this.minimalProcessorNumber = minimalProcessorNumber;
        return this;
    }

    public LPTSchedulerBuilder maximalProcessorNumber(final int maximalProcessorNumber) {
        this.maximalProcessorNumber = maximalProcessorNumber;
        return this;
    }

    public LPTSchedulerBuilder minimalLength(final int minimalLength) {
        this.minimalLength = minimalLength;
        return this;
    }

    public LPTSchedulerBuilder maximalLength(final int maximalLength) {
        this.maximalLength = maximalLength;
        return this;
    }

    public LPTSchedulerBuilder numberOfJobs(final int numberOfJobs) {
        this.numberOfJobs = numberOfJobs;
        return this;
    }

    public LPTSchedulerBuilder numberOfProcessors(final int numberOfProcessors) {
        this.numberOfProcessors = numberOfProcessors;
        return this;
    }

}
