package computationCenter;

import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>TestAll</code> builds a suite that can be used to run all of
 * the tests within its package as well as within any subpackages of its
 * package.
 * 
 * @generatedBy CodePro at 15/04/12 00:10
 * @author d
 * @version $Revision: 1.0 $
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ ProcessorTest.class, TaskTest.class, JobPackageTest.class, ClusterTest.class, })
public class TestAll {

    /**
     * Launch the test.
     * 
     * @param args
     *            the command line arguments
     * 
     * @generatedBy CodePro at 15/04/12 00:10
     */
    public static void main(final String[] args) {
        JUnitCore.runClasses(new Class[] { TestAll.class });
    }
}
