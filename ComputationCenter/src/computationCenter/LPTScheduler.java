package computationCenter;

import java.util.Iterator;

public class LPTScheduler {
    private final AllJobs allJobs;
    private Cluster cluster;
    private Metrics metric;
    private int longestIdleTimeOfProcessor;
    private int processorsWithIdleTime;

    public LPTScheduler(final int minimalProcessorNumber, final int maximalProcessorNumber, final int minimalLength,
            final int maximalLength, final int numberOfJobs, final int processorsInCluster) {
        final AllJobsBuilder ajb = new AllJobsBuilder();
        this.allJobs = ajb.minimalProcessorNumber(minimalProcessorNumber)
                .maximalProcessorNumber(maximalProcessorNumber).minimalLength(minimalLength)
                .maximalLength(maximalLength).numberOfJobs(numberOfJobs).buildAllJobs();

        this.cluster = new Cluster(processorsInCluster);

    }

    public void setProcessorNumber(final int numberOfProcessors) {
        this.cluster = new Cluster(numberOfProcessors);
    }

    public void increaseProcessorNumber(final int numberOfAdditionalProcessors) {
        final int currentNuber = this.cluster.getNumberOfProcessors();
        this.cluster = new Cluster(currentNuber + numberOfAdditionalProcessors);
    }

    public void decreaseProcessorNumber(final int numberOfProcessorsToRemove) {
        final int currentNuber = this.cluster.getNumberOfProcessors();
        if ((currentNuber - numberOfProcessorsToRemove) > 0) {
            this.cluster = new Cluster(currentNuber - numberOfProcessorsToRemove);
        }
        else {
            this.cluster = new Cluster(1);
        }

    }

    public void schedule() {
        final int numberOfProcessors = this.cluster.getNumberOfProcessors();
        final Iterator<JobPackage> iter = this.allJobs.iterator();
        while (iter.hasNext()) {
            final JobPackage jp = iter.next();
            final int requiredProcessors = jp.getRequredProcessors();
            jp.allocateTo(0, 0);

            while (!this.cluster.canYouAdd(jp)) {
                if (jp.getStartProcessor() == ((numberOfProcessors - requiredProcessors))) {
                    jp.moveForwardInTime();
                    jp.resetProcessor();
                    if (this.cluster.canYouAdd(jp)) {
                        break;
                    }
                }
                jp.moveToNextProcessor();
            }
            this.cluster.add(jp);
        }
        this.metric = new LongestIdleTimeOfProcessor();
        this.longestIdleTimeOfProcessor = this.metric.compute(this.cluster);
        this.metric = new ProcessorsWithIdleTime();
        this.processorsWithIdleTime = this.metric.compute(this.cluster);
    }

    public int getLongestIdleTimeOfProcessor() {
        return this.longestIdleTimeOfProcessor;
    }

    public int getProcessorsWithIdleTime() {
        return this.processorsWithIdleTime;
    }

    public int getArea() {
        return this.cluster.getClusterArea();
    }
}
